package com.androidz.coinshot;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Surface.OutOfResourcesException;

public class GameController extends SurfaceView implements SurfaceHolder.Callback {

	public final static String FONT1_PATH = "fonts/knuckledown.ttf";
	public final static int MAX_MEDIAPLAYERS = 4;
	
	protected static SurfaceHolder surface;
	protected static Context ctx;
	private MainThread mainThread;
	private CoinConfigThread coinThread;
	private ScoreThread scoreThread;
	private LoadingThread loadingThread;
	
	//TODO: level object/loader
	private Bitmap levelBackground;
	
	public boolean loadingScreenOn = false;
	
	public boolean surfaceReady = false;
	
	protected static boolean launchTest = false;
	
	public Typeface font1;
	
	protected static Point windowSize;
	
	private static Vibrator vibrator;
	private static Timer t;
	
	private static final int GAME_LENGTH_DEFAULT = 60;//seconds
	
	private static long	gunVibrateTime = 150;//ms
	private static long	gunVibrateTimeReload = 100;//ms
	
	public static int activeSoundCount = 0;
	//private static GunzSoundPlayer mpPool[] = new GunzSoundPlayer[MAX_MEDIAPLAYERS];
	
	private static int soundReloadId;
	private static int soundFireId;
	private static int soundScoreId;
	
	private static float currentScore = 0.00f;
	private static String currentScoreString = "0.00";
	
	private static SoundPool soundPool;
	
	protected int timeLeft;
	
	public GameController(Context context, AttributeSet attrs) {
		super(context, attrs);
		ctx = context;
		
		SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        init();
        
        resetGame();
	}
	
	public void init()
	{
		t = new Timer();
		
		vibrator = (Vibrator)ctx.getSystemService(Context.VIBRATOR_SERVICE);
		
		soundPool = new SoundPool(MAX_MEDIAPLAYERS,
				AudioManager.STREAM_MUSIC,
				0);

		font1 = Typeface.createFromAsset(ctx.getAssets(),
								FONT1_PATH);

		levelBackground = BitmapFactory.decodeResource(ctx.getResources(),
				R.drawable.bg1);
		
		mainThread = new MainThread();
		
		coinThread = new CoinConfigThread();
		
        scoreThread = new ScoreThread();
        
        loadingThread = new LoadingThread();
        
        loadingScreenOn = true;
        
        setFocusable(true);
	}

	public void resetGame(){
		timeLeft = GAME_LENGTH_DEFAULT;
		currentScore = 0;
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		surface = holder;
		surfaceReady = true;
		
		loadingThread.start();
		mainThread.setRunning(true);
		mainThread.start();
		coinThread.start();
		
		scoreThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		surfaceReady = false;
	}

	public static void onDischarge(int roundcount)
	{
		soundPool.play(soundFireId, 1.0f, 1.0f, 1, 0, 1.0f);
		
		t.schedule(new TimerTask() {
			public void run() {
				vibrator.vibrate(gunVibrateTime);
			}
		}, 1);
		
		updateRoundCount(roundcount);
	}
	
	public static void updateRoundCount(int roundcount)
	{
		CoinShot.mHandler.post(new ParamRunnable(Integer.toString(roundcount)) {
			@Override
			public void run() {
				CoinShot.mRoundCount.setText(this.getLabel());
			}
		});
	}
	
	
	public static float addToScore(float amount)
	{
		currentScore += amount;
		return currentScore;
	}
	
	public float getCurrentScore()
	{
		return currentScore;
	}
	
	public static void onReload() {
		soundPool.play(soundReloadId, 1.0f, 1.0f, 1, 0, 1.0f);
		t.schedule(new TimerTask() {
			public void run() {
				vibrator.vibrate(gunVibrateTimeReload);
			}
		}, 550);
	}
	
	public Context getCtx()
	{
		return ctx;
	}
	
	public void launchCoin()
	{
		//launchTest = true;
		//mainThread.coins[0].launch();
	}
	
	class CoinConfigThread extends Thread
	{
		public CoinConfigThread() {
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public void run() {
			while(mainThread.mRun)
			{
				Random r = new Random();
				float rand1 = r.nextFloat();
				float rand2 = r.nextFloat();
				float rand3 = r.nextFloat();
				
				for (int i = 0; i < mainThread.coins.length; i++) {
					if(mainThread.coins[i]!=null){
						if(!mainThread.coins[i].isMoving())
						{
							//auto-launch
							if(rand1>=0.45f){
								//change velocity and options
								if(rand2>=0.35f){
									PointF coinSpeed = new PointF();
									coinSpeed.y =  (float) ((Math.random()*10)+5);
									
									coinSpeed.x = -(rand1*2);
									if(rand1>=0.65f)
										coinSpeed.x = coinSpeed.x*-1;
									
									/*if(rand1>=0.5f){
										coinSpeed.x = coinSpeed.x*-1;
									} */
									
									float launchHeight = (rand2*150);
									if(rand1>=0.95f){
										launchHeight = launchHeight*-1;
									}
									
									PointF coinDest = new PointF(0.0f,launchHeight);
									
									mainThread.coins[i].setCoinSize((int) ((rand1*40)+50));
									mainThread.coins[i].setCoinSpeed(coinSpeed);
									//mainThread.coins[i].setCoinFriction((float) ((Math.random()*0.1f)+0.99f));
									mainThread.coins[i].setDestination(coinDest);
									mainThread.coins[i].setLaunchPoint(rand3*350);
									mainThread.coins[i].setCoinDelay((int) (rand3*80)+10);
								}
								
								mainThread.coins[i].launch();
								rand1 = r.nextFloat();
								rand2 = r.nextFloat();
								rand3 = r.nextFloat();
							}
						}
					}
				}
				
				try {
					Thread.sleep(700);
				} catch (InterruptedException e) {
				}
			}
		}
	}
	
	public boolean isHit(SpriteController object, PointF impact)
	{
		final double circleX;
		final double circleY;
		
		if(object.destroyObject)
			return false;//object is already destroyed
		
		if(impact.x > object.spriteLoc.x){
			if(impact.y > object.spriteLoc.y){
    			if(impact.x < object.spriteLoc.x+object.spriteSize.x){
    				if(impact.y < object.spriteLoc.y+object.spriteSize.y){
		
						try{
							circleX = object.spriteLoc.x + (object.spriteSize.x/2);
							circleY = object.spriteLoc.y + (object.spriteSize.y/2);
						} catch(NullPointerException e){
							return false;
						}
						
						Double d = Math.sqrt((impact.x-circleX)*(impact.x-circleX) + 
													(impact.y-circleY)*(impact.y-circleY));
						
						if(Firearm.DBG_MODE)
							CoinShot.log("Impact distance from circle center: "+d+" ("+circleX+","+circleY+")");
						
						if(d<=object.spriteSize.x){
							
							int spriteOffsetX = (int) (impact.x - object.spriteLoc.x);
							int spriteOffsetY = (int) (impact.y - object.spriteLoc.y);
							
							if(((SpriteCoin)object).getCurrentFrame().getPixel(spriteOffsetX, spriteOffsetY)
									!=Color.TRANSPARENT){
								if(Firearm.DBG_MODE)
									CoinShot.log("Impact!");
								return true;
							}
						}
    				}
    			}
			}
		}
		
		return false;
	}
	
	public boolean isFiringOrReloading()
	{
		if(Firearm.currentAnimationState!=Firearm.WEAPON_STATE_IDLE){
			return true;
		}
		return false;
	}
	
	public boolean isReloading()
	{
		if(Firearm.currentAnimationState==Firearm.WEAPON_STATE_RELOAD){
			return true;
		}
		return false;
	}
	
	/*public class scoreUpdate implements Runnable
	{
		public float score = 0.f;
		public scoreUpdate(float s) {
			score = s;
		}
	
		@Override
		public void run() {
			addToScore(score);
		}
	}*/
	
	private Runnable onScoreChange = new Runnable() {
		@Override
		public void run() {
			soundPool.play(soundScoreId, 1.0f, 1.0f, 1, 0, 1.0f);
		}
	};
	
	public void fireWeapon(PointF impact)
	{
		//CoinShot.log("got motion event at: "+impact.x+","+impact.y);
		if(mainThread.weapon.fire())
		{
			for (int i = 0; i < mainThread.coins.length; i++) {
				if(isHit(mainThread.coins[i],impact)){
					if(Firearm.DBG_MODE)
						CoinShot.log("Hit on target: "+i);
					mainThread.impact(i);
					currentScore += mainThread.coins[i].getValue();
					CoinShot.mHandler.postDelayed(onScoreChange, 500);
				}
			}
		}
	}
	
	class ScoreThread extends Thread
	{
		@Override
		public void run() {
			while(mainThread.mRun)
			{
				if(loadingScreenOn)
					continue;
				
				currentScoreString = String.format("%02.2f",currentScore);
				
				if(timeLeft>0){
					timeLeft--;
				} else if(timeLeft==0) {
					((CoinShot)ctx).onGameOver(currentScoreString);
					timeLeft = -1;
				}
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					break;
				}
			}
		}
	}
	
	class LoadingThread extends Thread {

		public boolean run = true;
		
		public LoadingThread() {
		}
		
		@Override
		public void run() {
			Bitmap loadingBg = BitmapFactory.decodeResource(ctx.getResources(),
												R.drawable.loading_bg);
			Rect r = new Rect(0,0,windowSize.x,windowSize.y);
			Point p = new Point(windowSize.x/2,windowSize.y/2);
			while(run){
				if(!loadingScreenOn)
					break;
				Canvas c = null;
				try {
	                synchronized (surface) {
	                	try {
							c = surface.lockCanvas(null);
						} catch (Exception e) {
							continue;
						}
	                	
	                	c.drawBitmap(loadingBg, null, r, null);
	                	
	                	if(mainThread.isLoadingCoinReady)
	                		mainThread.coins[0].renderInPlace(c, p);
	                	
	                }
	            } catch(NullPointerException e){ 
	            	break;
	            } finally {
	                // do this in a finally so that if an exception is thrown
	                // during the above, we don't leave the Surface in an
	                // inconsistent state
	                if (c != null) {
	                    surface.unlockCanvasAndPost(c);
	                }
	            }
			}
			loadingBg.recycle();
			loadingBg = null;
			r = null;
			loadingScreenOn = false;
			return;
		}
		
	}
	
	class MainThread extends Thread
	{
		private final static float DEFAULT_COIN_LAUNCHSPEED = 10.f;
		
		public boolean animationStopped = false;
		
		Bitmap aCoin[];
		protected boolean mRun = false;

		private PointF coinLoc;
		private PointF coinSpeed;
		private PointF coinDest;
		private float coinBaseline = 350.f;
		
		private boolean isLoadingCoinReady = false;
		
		public SpriteController[] coins = new SpriteController[3];
		
		public Firearm weapon;
		
		public void killYourself(){
			mRun = false;
			try {
				coinThread.join();
				scoreThread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			weapon.kill();
			for (SpriteController c : coins) {
				c.destroy();
			}
		}
		
		public MainThread() {
			
			weapon = new Firearm(ctx);
			
			coinBaseline = 550.f;
			
			coinLoc = new PointF(150.f, coinBaseline);
			coinSpeed = new PointF(0.0f,DEFAULT_COIN_LAUNCHSPEED);
			coinDest = new PointF(150.f,90.f);
			
			coins[0] = new SpriteCoin(ctx, R.xml.x_quarter);
			coins[0].configure(coinLoc, coinSpeed,coinDest);
			
			isLoadingCoinReady = true;
			
			coinLoc = new PointF(50.f, coinBaseline);
			coinDest = new PointF(80.f,30.f);
			
			coins[1] = new SpriteCoin(ctx, R.xml.x_quarter);
			coins[1].configure(new PointF(90.f, coinBaseline),
							new PointF(9.0f,24.f),
							coinDest);
			
			coins[1].setCoinDelay(10);
			coins[1].setCoinFriction(1.f);
			coins[1].setCoinSize(65);
			
			coins[2] = new SpriteCoin(ctx, R.xml.x_quarter);
			coins[2].configure(new PointF(7.f, coinBaseline),
								new PointF(9.0f,24.f),
								new PointF(80.f,30.f));
			coins[2].setCoinDelay(35);
			coins[2].setCoinFriction(0.99f);
			coins[2].setCoinSize(100);
		}
		
		public void doStart()
		{
			synchronized (surface) {
				
			}
		}
		
		private void renderAmmoCount(Canvas c)
		{
			
		}
		
		public void impact(int coinidx)
		{
			try{
				coins[coinidx].impact();
			} catch(NullPointerException e){}
		}
		
		@Override
		public void run()
		{
			Paint p = new Paint();
			p.setTypeface(font1);
			p.setTextSize(24.0f);
			p.setColor(Color.GREEN);
			p.setAntiAlias(true);
			p.setShadowLayer(5.0f, 0.1f, 0.1f, Color.LTGRAY);
			
			windowSize = new Point(getWidth(),getHeight());
			Rect r = new Rect(0,0,windowSize.x,windowSize.y);
			
			weapon.loadPackage(CoinShot.gunId, GameController.this);
			String soundFireAsset = FireArms.getSoundPath(weapon.gunPath, FireArms.GUNZ_ZPACKAGE_FILENAME_SOUND_SHOOT); 
			String soundReloadAsset = FireArms.getSoundPath(weapon.gunPath, FireArms.GUNZ_ZPACKAGE_FILENAME_SOUND_RELOAD); 
			
			soundFireId = soundPool.load(soundFireAsset, 1);
			soundReloadId = soundPool.load(soundReloadAsset, 1);
			soundScoreId = soundPool.load(ctx, R.raw.kaching, 1);
			
			((CoinShot)ctx).hideLoadingScreen();
			
			while (mRun) {
				if(loadingScreenOn)
					continue;
                Canvas c = null;
                try {
                    synchronized (surface) {
                    	c = surface.lockCanvas(null);
                    	
                    	c.drawColor(Color.BLACK);
                    	
                    	c.drawBitmap(levelBackground, null, r, null);
                    	
                    	c.drawText("$"+currentScoreString, 245, 25, p);
                    	c.drawText("-"+Integer.toString(timeLeft)+"-", 5, 25, p);
                    	
                    	for (int i = 0; i < coins.length; i++) {
							if(coins[i]!=null){
								coins[i].render(c);
							}
						}
                    	
                    	weapon.render(c);
                    }
                } catch(NullPointerException e){ 
                	break;
                } finally {
                    // do this in a finally so that if an exception is thrown
                    // during the above, we don't leave the Surface in an
                    // inconsistent state
                    if (c != null) {
                        surface.unlockCanvasAndPost(c);
                    }
                }
            }

			return;
		}
		
		public void setRunning(boolean b) {
            mRun = b;
        }

	}

	public void hideLoadingScreen()
	{
		loadingThread.run = false;
		try {
			loadingThread.join();
		} catch (InterruptedException e) {
		}
		
		loadingThread = null;
	}
	
	public void kill() {
		
		mainThread.killYourself();
		mainThread = null;
		scoreThread = null;
		coinThread = null;
		soundPool.release();
		soundPool = null;
		levelBackground.recycle();
		levelBackground = null;
		font1 = null;
		vibrator = null;
		t.cancel();
		t = null;
	}

	public void reload() {
		mainThread.weapon.reload();
	}
}
