package com.androidz.coinshot;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import android.os.Environment;
import android.util.Log;

public class FireArms
{
	public final static String STATIC_GUNLABEL = "meu";
	
	public final static String GUNZ_GUN_DIRECTORY_GLOBAL = "/GunZ/Guns";
	public final static String GUNZ_GUN_DIRECTORY_CACHE = "cache/";
	
	public final static String GUNZ_GUN_ANIMATION_EXTENSION = ".png";
	public final static String GUNZ_GUN_ANIMATION_PREFIX    = "z_anim_";
	
	public final static String GUNZ_ZPACKAGE_PATH_RELOAD = "reload/";
	public final static String GUNZ_ZPACKAGE_PATH_SHOOT  = "shoot/";
	public final static String GUNZ_ZPACKAGE_PATH_SOUNDS = "sounds/";
	
	public final static String GUNZ_GUN_FILENAME_ZPACKAGE = "gun.zip";
	public final static String GUNZ_ZPACKAGE_FILENAME_CONFIG = "config.xml";
	public final static String GUNZ_ZPACKAGE_FILENAME_AXML = "animation.xml";
	public final static String GUNZ_ZPACKAGE_FILENAME_SOUND_SHOOT  = "shoot1.mp3";
	public final static String GUNZ_ZPACKAGE_FILENAME_SOUND_RELOAD = "reload.mp3";
	public final static String GUNZ_ZPACKAGE_FILENAME_SOUND_SLIDEBACK = "slideback.wav";
	
	public final static String GUNZ_ZPACKAGE_ROOT = "gun/";
	
	protected static final int WEAPONID_MEU = 1;
	protected static final int WEAPONID_USP45 = 2;
	protected static final int WEAPONID_T850 = 3;
	protected static final int WEAPONID_DEAGLE = 4;
	
	private static final String CONFIG_TAGNAME_MAGAZINECAPACITY = "magazineCapacity";
	private static final String CONFIG_TAGNAME_DISCHARGEANIMS = "dischargeAnimations";
	private static final String CONFIG_TAGNAME_MFLASHOFFSETX = "muzzleFlashOffsetX";
	private static final String CONFIG_TAGNAME_MFLASHOFFSETY = "muzzleFlashOffsetY";
	private static final String CONFIG_TAGNAME_MFLASHENABLED = "hasMuzzleFlash";
	
	protected static final int DOUBLETAP_DELAY_PISTOL = 50;//ms
	
	public static final int ANIM_LEVEL_FIRST = 1;
	
	/**
	 * gets the full path to save a particular guns universal package.
	 * @return
	 */
	public static String getUniveralPackagePath(String gunid)
	{
		String coinshotDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		coinshotDir += GUNZ_GUN_DIRECTORY_GLOBAL+"/"+gunid+"/";
		return coinshotDir;
	}
	
	public static boolean extractPackage(String gundir)
	{
		return(extractPackage(gundir,false));
	}
	
	public static String getSoundPath(String gundir, String filename)
	{
		String ret = gundir+GUNZ_GUN_DIRECTORY_CACHE + GUNZ_ZPACKAGE_PATH_SOUNDS+filename;
		File f = new File(ret);
		
		if(f.exists() && f.canRead()){
			return ret;
		}
		
		return null;
	}
	
	public static boolean extractPackage(String gundir, boolean refresh)
	{
		try {
			
			String cacheDir = gundir+GUNZ_GUN_DIRECTORY_CACHE;
			File fcacheDir = new File(cacheDir);
			
			if(!fcacheDir.isDirectory()){
				fcacheDir.mkdirs();
			}
			
			if((new File(cacheDir+GUNZ_ZPACKAGE_FILENAME_CONFIG).isFile())){
				if(refresh){
					//clean out existing cache
					Util.deleteDir(fcacheDir);
					fcacheDir.mkdirs();
				} else {
					//package already seems to still be extracted from last go
					return true;
				}
			}
			
			ZipFile z = new ZipFile(gundir+GUNZ_GUN_FILENAME_ZPACKAGE);
			
			Enumeration<? extends ZipEntry> entries = z.entries();
			
			while(entries.hasMoreElements()){
				
				ZipEntry entry = (ZipEntry)entries.nextElement();

				String dest = cacheDir+entry.getName().replace(GUNZ_ZPACKAGE_ROOT, "");
				
				if(entry.isDirectory()){
					if(CoinShot.DBG_MODE)
						CoinShot.log("unpacking directory: "+entry.getName());
					if(!entry.getName().equals(GUNZ_ZPACKAGE_ROOT))
						(new File(dest)).mkdir();
					continue;
				}
				
				if(CoinShot.DBG_MODE)
					CoinShot.log("unpacking file: "+entry.getName());
				
				Util.copyInputStream(z.getInputStream(entry),
						new BufferedOutputStream(new FileOutputStream(dest)));
			}
			
			z.close();
			
			return true;
			
		} catch (Exception e) {
			if(CoinShot.DBG_MODE)
				e.printStackTrace();
		}
		return false;
	}
	
	public static FirearmConfig parseConfig(String gundir){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		FirearmConfig retObj = new FirearmConfig();
		
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			if(Firearm.DBG_MODE)
				e1.printStackTrace();
			builder = null;
		}
		
		if(builder==null)
			return retObj;
		
		try {
			
			String cacheDir = gundir+GUNZ_GUN_DIRECTORY_CACHE;
			File fcacheDir = new File(cacheDir);
			
			if(!fcacheDir.isDirectory()){
				fcacheDir.mkdirs();
			}
			
			File gunConfig = new File(cacheDir+GUNZ_ZPACKAGE_FILENAME_CONFIG);
			
			if(!gunConfig.isFile())
				return retObj;
			
			//int resId = AndroidGunz.getCtx().getResources().getIdentifier(ident, "raw", AndroidGunz.getCtx().getPackageName());
			Document document = builder.parse(gunConfig);
			
			String magCapacity = document.getElementsByTagName(CONFIG_TAGNAME_MAGAZINECAPACITY).item(0).getFirstChild().getNodeValue();
			String dischargeAnimations = document.getElementsByTagName(CONFIG_TAGNAME_DISCHARGEANIMS).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetX = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHOFFSETX).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetY = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHOFFSETY).item(0).getFirstChild().getNodeValue();
			String mFlashEnabled = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHENABLED).item(0).getFirstChild().getNodeValue();
			
			if(Firearm.DBG_MODE){
				Log.i("Firearm","Loading config");
				Log.i("Firearm","Found magazine capacity: "+magCapacity);
				Log.i("Firearm","Found discharge anims: "+dischargeAnimations);
				Log.i("Firearm","Found muzzle flash offset x: "+mFlashOffsetX);
				Log.i("Firearm","Found muzzle flash offset y: "+mFlashOffsetY);
			}
			
			retObj.magazineCap 		  = Integer.parseInt(magCapacity);
			retObj.dischareAnimations = Integer.parseInt(dischargeAnimations);
			retObj.muzzleFlashOffsetX = Integer.parseInt(mFlashOffsetX);
			retObj.muzzleFlashOffsetY = Integer.parseInt(mFlashOffsetY);
			retObj.hasMuzzleFlash 	  = mFlashEnabled.equals("false")?false:true;
			
		} catch (Exception e) {
			if(Firearm.DBG_MODE)
				e.printStackTrace();
			factory = null;
		}
		factory = null;
		
		return retObj;
	}
	
}