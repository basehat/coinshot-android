package com.androidz.coinshot;

public class FirearmConfig {
	
	protected static final boolean DEFAULT_AUTORELOAD = true;
	protected static final boolean DEFAULT_JOHNWOO 	  = false;
	protected static final boolean DEFAULT_FACEDETECT = false;
	protected static final boolean DEFAULT_HEADSHOTS = false;
	protected static final boolean DEFAULT_HITBOXES = false;
	protected static final boolean DEFAULT_MUSIC_REPEAT = false;
	
	protected int magazineCap;
	protected int dischareAnimations;
	protected int muzzleFlashOffsetX;
	protected int muzzleFlashOffsetY;
	protected boolean johnWoo = FirearmConfig.DEFAULT_JOHNWOO;
	protected boolean autoReload = FirearmConfig.DEFAULT_AUTORELOAD;
	protected boolean hasMuzzleFlash = true;
	public boolean hasShootlastAnimation = false;
}
