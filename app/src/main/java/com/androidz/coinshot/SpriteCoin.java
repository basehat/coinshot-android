package com.androidz.coinshot;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;

public class SpriteCoin extends SpriteController {

	public final static float DEFAULT_VALUE = 0.25f;
	
	//each object type we are using has only one set of bitmaps
	protected static Bitmap mainAnimation[];
	protected static Bitmap impactAnimation[] = null;

	public float coinPointsValue = 0.25f;
	
	public SpriteCoin(Context c, int resId) {
		super(c);
		
		spriteLoc = new PointF(spriteBaseline.x, spriteBaseline.y);
		spriteSpeed = new PointF(0.0f,DEFAULT_OBJECT_LAUNCHSPEED);
		spriteDest = new PointF(150.f,20.f);
		
		if(mainAnimation==null)
			mainAnimation = Util.getAnimationFrames(ctx, resId);
		
		if(impactAnimation==null)
		{
			impactAnimation = Util.getAnimationFrames(ctx, R.xml.x_coinexplode);
		}
		
		impactSize.x = impactAnimation[0].getWidth();
		impactSize.y = impactAnimation[0].getHeight();
		
		impactOffset.x = impactSize.x/2;
		impactOffset.y = impactSize.y/2;
	}
	
	@Override
	public void setCoinSize(int size) {
		
		spriteSize = new Point(size,size);
		
		float scale = (size/mainAnimation[0].getWidth())*0.85f;
		
		//impactSize.x = impactAnimation[0].getWidth();
		//impactSize.y = impactAnimation[0].getHeight();
			
		impactOffset.x = impactSize.x/2;
		impactOffset.y = impactSize.y/2;
	}
	
	@Override
	void onDestructionCycle(Canvas c) {
		//coin was shot divert to destruction
		if(currentFrame>=impactAnimation.length){
			currentFrame = 0;
			resetOptions();
			destroyObject = false;
    	} else {
			c.save();
			float offsetX = spriteLoc.x-impactOffset.x;
			float offsetY = spriteLoc.y-impactOffset.y;
			
			c.rotate(impactRotation,offsetX+impactOffset.x,
									offsetY+impactOffset.y);
			if(spriteSize==null){
				c.drawBitmap(impactAnimation[currentFrame],
						offsetX,
						offsetY, null);
			} else {
				int left = (int)(spriteLoc.x-impactOffset.x);
    			int top = (int)(spriteLoc.y-impactOffset.y);
    			
    			Rect r= new Rect(left, top,
    								left+impactSize.x,
    								top+impactSize.y);
        		c.drawBitmap(impactAnimation[currentFrame], null, r, null);
			}
    		c.restore();
    	}
	}

	public Bitmap getCurrentFrame(){
		return mainAnimation[currentFrame];
	}
	
	@Override
	public void destroy() {
		super.destroy();
		if(mainAnimation!=null && mainAnimation.length>0)
		{
			for (int i = 0; i < mainAnimation.length; i++) {
				mainAnimation[i].recycle();
				mainAnimation[i] = null;
			}
			mainAnimation = null;
		}
	}
	
	@Override
	public float getValue() {
		return DEFAULT_VALUE;
	}

	@Override
	void onThrowCycle(Canvas c) {
		if(currentFrame>=mainAnimation.length){
    		currentFrame = 0;
    	}
    	
    	if(spriteSize==null)
    		c.drawBitmap(mainAnimation[currentFrame], spriteLoc.x, spriteLoc.y, null);
    	else {
    		Rect r= new Rect((int)spriteLoc.x,(int)spriteLoc.y,(int)spriteLoc.x+spriteSize.x,(int)spriteLoc.y+spriteSize.y);
    		c.drawBitmap(mainAnimation[currentFrame], null, r, null);
    	}
	}
	
	@Override
	void onStillRenderCycle(Canvas c, Point location) {
		if(currentFrame>=mainAnimation.length){
    		currentFrame = 0;
    	}
		
		c.drawBitmap(mainAnimation[currentFrame], location.x-(mainAnimation[0].getWidth()/2),
														location.y-(mainAnimation[0].getHeight()/2), null);
	}
}
