package com.androidz.coinshot;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Xml;

public class Util {
	
	private static final String ANIMATION_FRAME_TAG = "frame";
	
	public static float pixelsToDip(Context c, int px){
		float dip = (px * c.getResources().getDisplayMetrics().density + 0.5f);
		return dip;
	}
	
	public static Bitmap[] getDefaultMuzzleFlashBitmap(Context c)
	{
		Bitmap[] ret = new Bitmap[2];
		
		ret[0] = ((BitmapDrawable)c.getResources().getDrawable(R.drawable.muzzleflash1)).getBitmap();
		ret[1] = ((BitmapDrawable)c.getResources().getDrawable(R.drawable.muzzleflash2)).getBitmap();
		
		return ret;
	}
	
	public static String[] parseXmlAnimation(Context c, int xmlResId){
		
		XmlResourceParser xr = c.getResources().getXml(xmlResId);
		ArrayList<String> ret = new ArrayList<String>();
		
		try {
			while (xr.getEventType() != XmlPullParser.END_DOCUMENT) {
				if (xr.getEventType() == XmlPullParser.START_TAG) {
							if(xr.getName().equals(ANIMATION_FRAME_TAG)){
								ret.add(xr.getAttributeValue(null, "drawable"));
							}
				        } else if (xr.getEventType() == XmlPullParser.END_TAG) {
				        }
				xr.next();
			}
		} catch (XmlPullParserException e) {
			if(CoinShot.DBG_MODE)
				e.printStackTrace();
		} catch (IOException e) {
			if(CoinShot.DBG_MODE)
				e.printStackTrace();
		}
		
		xr.close();
		
		return ret.toArray(new String[0]);
	}
	
	public static String[] parseXmlAnimation(File f)
		throws Exception
	{
		ArrayList<String> ret = new ArrayList<String>();
		FileInputStream fis = new FileInputStream(f);
		XmlPullParser xr = Xml.newPullParser();
	
		xr.setInput(fis, null);
		
		while (xr.getEventType() != XmlPullParser.END_DOCUMENT) {
			if (xr.getEventType() == XmlPullParser.START_TAG) {
						if(xr.getName().equals(ANIMATION_FRAME_TAG)){
							ret.add(xr.getAttributeValue(null, "drawable"));
						}
			        } else if (xr.getEventType() == XmlPullParser.END_TAG) {
			        }
			xr.next();
		}
		
		fis.close();
		
		return ret.toArray(new String[0]);
	}
	
	public static Bitmap[] getAnimationFrames(Context c, int xmlResId)
	{
		String[] frameNames = Util.parseXmlAnimation(c, xmlResId);
		Bitmap ret[] = new Bitmap[frameNames.length]; 
		int x = 0;
		for (String s : frameNames) {
			if(s!=null){
				int resId = c.getResources().getIdentifier(s, "drawable", c.getPackageName());
				ret[x] = BitmapFactory.decodeResource(c.getResources(), resId);
				x++;
			}
		}
		
		return ret;
	}
	
	public static Bitmap[] getAnimationFromDisk(Context c, String gundir, String stageFolder)
		throws Exception
	{
		String path = gundir+FireArms.GUNZ_GUN_DIRECTORY_CACHE;
		
		File xmlConfig = new File(path+FireArms.GUNZ_ZPACKAGE_FILENAME_CONFIG);
		if(!xmlConfig.isFile()){
			throw new Exception(xmlConfig.getAbsolutePath()+" does not exist!");
		}
		
		path += stageFolder;
		
		File xmlAnimation = new File(path+FireArms.GUNZ_ZPACKAGE_FILENAME_AXML);
		if(!xmlAnimation.isFile()){
			throw new Exception(xmlAnimation.getAbsolutePath()+" does not exist!");
		}
		
		String[] frameNames = Util.parseXmlAnimation(xmlAnimation);
		
		Bitmap[] ret = new Bitmap[frameNames.length]; 
		int x = 0;
		for (String s : frameNames) {
			if(s!=null){
				String tmpPath = path+s+FireArms.GUNZ_GUN_ANIMATION_EXTENSION;
				ret[x] = BitmapFactory.decodeFile(tmpPath);
				x++;
			}
		}
		
		return ret;
	}
	
	public static boolean deleteDir(File dir)
	{
		if (dir.isDirectory())
		{ 
			String[] children = dir.list();
			for (int i=0; i<children.length; i++) { 
				boolean success = deleteDir(new File(dir, children[i])); 
				if (!success)
						return false;
			}
		}
		// The directory is now empty so delete it
		return dir.delete(); 
	}
	
	public static void copyInputStream(InputStream in, OutputStream out)
	  throws IOException
	  {
	    byte[] buffer = new byte[1024];
	    int len;

	    while((len = in.read(buffer)) >= 0)
	      out.write(buffer, 0, len);

	    in.close();
	    out.close();
	  }
	
}
