package com.androidz.coinshot;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;

public class SpriteDroid extends SpriteController {
	
	protected static Bitmap mainAnimation[];
	protected static Bitmap impactAnimation[] = null;
	
	private static Paint p = null;
	
	public SpriteDroid(Context c, int resId) {
		super(c);
		
		
		spriteLoc = new PointF(spriteBaseline.x, spriteBaseline.y);
		spriteSpeed = new PointF(0.0f,DEFAULT_OBJECT_LAUNCHSPEED);
		spriteDest = new PointF(150.f,20.f);
		
		if(mainAnimation==null)
			mainAnimation = Util.getAnimationFrames(ctx, resId);
		
		if(impactAnimation==null)
		{
			impactAnimation = Util.getAnimationFrames(ctx, R.xml.x_explodeandroid);
			
			impactSize.x = impactAnimation[0].getWidth();
			impactSize.y = impactAnimation[0].getHeight();
			
			impactOffset.x = impactSize.x/2;
			impactOffset.y = impactSize.y/2;
		}
		
		if(p==null){
			p = new Paint();
			p.setAntiAlias(true);
		}
		
	}

	@Override
	public void setCoinSize(int size) {

		spriteSize = new Point(size,size);
		
		//float scale = (size/mainAnimation[0].getWidth())*0.75f;
		
		//impactSize.x = (int) (impactAnimation[0].getWidth()*scale);
		//impactSize.y = (int) (impactAnimation[0].getHeight()*scale);
			
		//impactOffset.x = impactSize.x/2;
		//impactOffset.y = impactSize.y/2;
	}
	
	@Override
	void onDestructionCycle(Canvas c) {
		//coin was shot divert to destruction
		if(currentFrame>=impactAnimation.length){
			destroyObject = false;
			currentFrame = 0;
			resetOptions();
    	} else {
    		//p.setAlpha(255-currentFrame);
    		//spriteLoc.y += currentFrame;
    		
    		c.drawBitmap(impactAnimation[currentFrame],
					spriteLoc.x-impactOffset.x,
					spriteLoc.y-impactOffset.y, p);
    		
    		/*if(spriteSize==null) {
    			c.drawBitmap(impactAnimation[currentFrame],
    					spriteLoc.x-impactOffset.x,
    					spriteLoc.y-impactOffset.y, p);
    		} else {
    			int left = (int)(spriteLoc.x-impactOffset.x);
    			int top = (int)(spriteLoc.y-impactOffset.y);
    			
    			Rect r= new Rect(left, top,
    								left+impactSize.x,
    								top+impactSize.y);
        		c.drawBitmap(impactAnimation[currentFrame], null, r, p);
    		}*/
    	}
	}

	@Override
	void onThrowCycle(Canvas c) {
		if(currentFrame>=mainAnimation.length){
    		currentFrame = 0;
    	}
    	
    	if(spriteSize==null)
    		c.drawBitmap(mainAnimation[currentFrame], spriteLoc.x, spriteLoc.y, null);
    	else {
    		Rect r= new Rect((int)spriteLoc.x,(int)spriteLoc.y,(int)spriteLoc.x+spriteSize.x,(int)spriteLoc.y+spriteSize.y);
    		c.drawBitmap(mainAnimation[currentFrame], null, r, null);
    	}
	}

	@Override
	void onStillRenderCycle(Canvas c, Point location) {
		// TODO Auto-generated method stub
		
	}
}
