package com.androidz.coinshot;

import java.io.File;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;

class Firearm extends SpriteController{
	protected static final boolean DBG_MODE = false;
	
	protected int gunId;
	protected int gunDischargeAnimsCount = 1;
	protected FirearmConfig config = null;
	
	protected static final float ANIMATION_DELAY = 25.0f;
	
	protected static final int FLASH_LENGTH = 2;
	
	protected static final String ANIM_STAGE_SHOOT = "shoot";
	protected static final String ANIM_STAGE_SHOOTLAST = "shootlast";
	protected static final String ANIM_STAGE_DRAW = "draw";
	protected static final String ANIM_STAGE_RELOAD = "reload";
	protected static final String ANIM_SOUND_SLIDEBACK = "slideback";
	
	public static final int WEAPON_STATE_IDLE = 0;
	public static final int WEAPON_STATE_FIRE = 1;
	public static final int WEAPON_STATE_FIRELAST = 2;
	public static final int WEAPON_STATE_RELOAD = 3;
	public static final int WEAPON_STATE_SLIDEBACK = 4;
	
	public static final int DEFAULT_DISCHARGE_FRAME = 1;
	public static final int DEFAULT_RELOAD_FRAME = 1;
	
	protected static int currentAnimationState = WEAPON_STATE_IDLE;
	private static int currentAnimationFrame = 0;
	private static int currentFlashFrame = 0;
	private static int flashFrameCount = 0;
	
	public int roundsLeft = 0;
	
	public Bitmap fireAnimation[];
	public Bitmap flashAnimation[];
	public Point fireAnimationOffset;
	public Bitmap reloadAnimation[];
	public Point reloadAnimationOffset;
	public Point flashOffset;
	public PointF flashRotatePivot;
	
	private Paint mPaint;
	
	private boolean isReady = false;
	public String gunPath = null;
	
	//private Random randObj = new Random();
	//private float nextRand = 0.f;
	private float rotateRand1 = 90.f;
	
	public Firearm(Context c) {
		super(c);
		
		mPaint = new Paint();
		mPaint.setAntiAlias(false);
		mPaint.setDither(true);
		
		//nextRand = (float)(Math.random()*360);//randObj.nextFloat();
	}
	
	@Override
	public void render(Canvas c)
	{
		if(!isReady)
			return;
		
		long now = System.currentTimeMillis();
    	double elapsed = (now - mLastTime);
    	mLastTimeFrame += elapsed;
    	elapsed = elapsed/1000.0;
    	
    	switch(currentAnimationState)
    	{
    		case(WEAPON_STATE_IDLE):
    			c.drawBitmap(fireAnimation[0], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    			break;
    		case(WEAPON_STATE_RELOAD):
    			mLastTimeFrame += elapsed;
			
				if(mLastTimeFrame>ANIMATION_DELAY)
				{
					currentAnimationFrame++;
					mLastTimeFrame = 0;
				}
				
				if(currentAnimationFrame>=reloadAnimation.length)
    			{
    				currentAnimationState = WEAPON_STATE_IDLE;
    				roundsLeft = config.magazineCap;
    				currentAnimationFrame = 0;
    				c.drawBitmap(fireAnimation[0], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    				GameController.updateRoundCount(roundsLeft);
    				break;
    			} else if(currentAnimationFrame==DEFAULT_RELOAD_FRAME){
    				GameController.onReload();
    				currentAnimationFrame++;
    			}
				
				c.drawBitmap(reloadAnimation[currentAnimationFrame], reloadAnimationOffset.x, reloadAnimationOffset.y, mPaint);
    			break;
    		case(WEAPON_STATE_FIRE):
    			
    			mLastTimeFrame += elapsed;
    			
    			if(mLastTimeFrame>ANIMATION_DELAY)
    			{
    				currentAnimationFrame++;
    				mLastTimeFrame = 0;
    			}
    			
    			if(currentAnimationFrame>=fireAnimation.length)
    			{
    				currentAnimationState = WEAPON_STATE_IDLE;
    				c.drawBitmap(fireAnimation[0], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    				break;
    			}
    			else if(currentAnimationFrame==DEFAULT_DISCHARGE_FRAME)
    			{
    				GameController.onDischarge(roundsLeft);
    				currentAnimationFrame++;
    				flashFrameCount = 0;
    			}
    			
    			if(flashFrameCount<=FLASH_LENGTH)
				{
					if(currentFlashFrame==flashAnimation.length){
						currentFlashFrame = 0;
					}
					
					c.save();
					c.rotate(rotateRand1, flashRotatePivot.x,
										  flashRotatePivot.y);
					c.drawBitmap(flashAnimation[currentFlashFrame], flashOffset.x, flashOffset.y, null);
					c.restore();
					
					currentFlashFrame++;
					flashFrameCount++;
				}
    			
    			c.drawBitmap(fireAnimation[currentAnimationFrame], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    			break;
    	}
    	
    	mLastTime = System.currentTimeMillis();
	}
	
	public boolean fire()
	{
		if(currentAnimationState==WEAPON_STATE_RELOAD)
			return false;
		
		if(roundsLeft>0){
			flashFrameCount = 99;
			currentFlashFrame = 0;
			startAnimation(WEAPON_STATE_FIRE);
			roundsLeft--;
			return true;
		} else {
			//nextRand = (float)(Math.random()*360);
			rotateRand1 = (float)(Math.random()*360);
			startAnimation(WEAPON_STATE_RELOAD);
		}
		return false;
	}
	
	public void reload()
	{
		roundsLeft = 0;
		this.fire();
	}
	
	private void startAnimation(int state)
	{
		currentAnimationState = state;
		mLastTimeFrame = 0;
		currentAnimationFrame = 0;//refire while firing, COD style
	}
	
	public boolean loadPackage(int gunid, GameController gc)
	{
		return(loadPackage(Integer.toString(gunid),gc));
	}
	
	public boolean loadPackage(String gunid, GameController gc)
	{
		String path = FireArms.getUniveralPackagePath(gunid);
		
		if(DBG_MODE)
			CoinShot.log("Loading package "+gunid+" from path: "+path);
		
		isReady = false;
		
		if(!(new File(path).isDirectory())){
			if(DBG_MODE)
				CoinShot.log("Path does not exist!: "+path);
			return false;
		}
		
		if(FireArms.extractPackage(path))
		{
			gunPath = path;
			
			config = FireArms.parseConfig(path);
			
			try {
				fireAnimation =  Util.getAnimationFromDisk(ctx, gunPath, FireArms.GUNZ_ZPACKAGE_PATH_SHOOT);
				fireAnimationOffset = new Point(gc.getWidth()-fireAnimation[0].getWidth(),
												gc.getHeight()-fireAnimation[0].getHeight());
				
				reloadAnimation =  Util.getAnimationFromDisk(ctx, gunPath, FireArms.GUNZ_ZPACKAGE_PATH_RELOAD);
				reloadAnimationOffset = new Point(gc.getWidth()-reloadAnimation[0].getWidth(),
						gc.getHeight()-reloadAnimation[0].getHeight());
				
				if(config.hasMuzzleFlash){
					flashAnimation = Util.getDefaultMuzzleFlashBitmap(ctx);
				}
				
			} catch (Exception e) {
				if(DBG_MODE){
					CoinShot.log("error loading animation from disk!");
					e.printStackTrace();
				}
				return false;
			}
			
			//TODO: preload all assets
			
			//setup gun-specific configs
			roundsLeft = config.magazineCap;
			flashOffset = new Point();
			flashOffset.x = (int) Util.pixelsToDip(ctx, config.muzzleFlashOffsetX);
			flashOffset.y = (int) Util.pixelsToDip(ctx, config.muzzleFlashOffsetY+fireAnimationOffset.y);
			
			flashRotatePivot = new PointF(flashOffset.x+flashAnimation[0].getWidth()/2,
											flashOffset.y+flashAnimation[0].getHeight()/2);
			
			GameController.updateRoundCount(roundsLeft);
			
			isReady = true;
			return true;
		}
		
		return false;
	}
	
	public void kill(){
		isReady = false;
		fireAnimation = null;
		reloadAnimation = null;
		flashAnimation = null;
	}
	
	public boolean preLoad()
	{
		if(gunPath==null || gunPath.length()==0){
			return false;
		}
		
		isReady = false;
		
		
		
		
		return false;
	}

	public boolean isReady() {
		return isReady;
	}

	@Override
	void onDestructionCycle(Canvas c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void onThrowCycle(Canvas c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCoinSize(int size) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void onStillRenderCycle(Canvas c, Point location) {
		// TODO Auto-generated method stub
	}
	
	
}
