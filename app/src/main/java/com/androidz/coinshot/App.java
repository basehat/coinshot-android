package com.androidz.coinshot;

import java.util.HashMap;
import java.util.Map;

import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;

import android.app.Application;

public class App extends Application {

	public static final String gameName = "CoinShot!";
	public static final String gameID = "254433";
	public static final String gameKey = "TCcvnvHyMAreR3wAqdFMQ";
	public static final String gameSecret = "QFd2MFqY0O3C1ux3Rl0spyQ59d1vitvu26pqsO2MM";
	 
    @Override
    public void onCreate() {
       super.onCreate();

       Map<String, Object> options = new HashMap<String, Object>();
       options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
       // use the below line to set orientation
       // options.put(OpenFeintSettings.RequestedOrientation, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
       OpenFeintSettings settings = new OpenFeintSettings(gameName, gameKey,
       		gameSecret, gameID, options);
       
       OpenFeint.initialize(this, settings, new OpenFeintDelegate() { });
    }
	
}
