package com.androidz.coinshot;

import java.io.File;

import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PointF;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CoinShot extends Activity {
	
	protected static boolean DBG_MODE = true;
	
	public final static String LEADERBOARD_ID_TOPSHOOTERS = "669756";
	
	private GameController gameView;
	
	protected static Handler mHandler = new Handler();
	protected static TextView mRoundCount;
	
	protected static int gunId = MenuActivity.GUNID_DEFAULT;
	
//	protected RelativeLayout loadingOverlay;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        gunId = getIntent().getExtras().getInt(MenuActivity.INTENT_EXTRA_GUNID);
        
        setContentView(R.layout.main);
        
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        firstRun();
        
        mRoundCount = (TextView)findViewById(R.id.roundcount);
        
       // loadingOverlay = (RelativeLayout)findViewById(R.id.loadingoverlay);
        //loadingOverlay.setVisibility(View.VISIBLE);
        
        gameView = (GameController)findViewById(R.id.gameview);
        
        gameView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction()==MotionEvent.ACTION_DOWN)
				{
					gameView.fireWeapon(new PointF(event.getX(),event.getY()));
					return true;
				}
				return false;
			}
		});
        
        ImageView bReload = (ImageView)findViewById(R.id.b_reload);
        bReload.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction()==MotionEvent.ACTION_DOWN)
				{
					gameView.reload();
					return true;
				}
				return false;
			}
		});
    }
    
    @Override
    protected void onPause() {
    	gameView.kill();
    	super.onPause();
    }
    
    @Override
    protected void onResume() {
    	//gameView = (GameController)findViewById(R.id.gameview);
    	gameView.init();
    	super.onResume();
    }
    
    public void hideLoadingScreen(){
    	mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				gameView.hideLoadingScreen();
			}
		}, 3000);
    }
    
    public void onGameOver(String score){
    	
    	mHandler.post(new ParamRunnable(score) {
			@Override
			public void run() {
				TextView tv = new TextView(CoinShot.this);
	        	tv.setText(CoinShot.this.getString(R.string.t_gameover_score)+" $"+getLabel());
	        	tv.setGravity(Gravity.CENTER);
	        	tv.setTextSize(19);
	        	tv.setTextColor(Color.WHITE);
	        	
	        	TextView title = new TextView(CoinShot.this);
	        	title.setText(R.string.t_gameover_title);
	        	title.setGravity(Gravity.CENTER);
	        	title.setTextSize(24);
	        	title.setTextColor(Color.WHITE);
	        	
	        	AlertDialog.Builder builder = new AlertDialog.Builder(CoinShot.this);
	        	builder.setView(tv)
	        			.setCustomTitle(title)
	        	       .setCancelable(false)
	        	       .setPositiveButton(R.string.t_gameover_submit, new DialogInterface.OnClickListener() {
	        	           public void onClick(DialogInterface dialog, int id) {
	        	        	   
	        	        	   String tmpString = String.format("$%02.2f",gameView.getCurrentScore());
	        	        	   String gunList[] = getResources().getStringArray(R.array.guns);
	        	        	   String tmpGunName = CoinShot.this.getString(R.string.t_label_gun)+" ";
	        	        	   
	        	        	   for (String s : gunList) {
	        	        		   String tmpOpts[] = s.split(";");
	        	        		   if(Integer.parseInt(tmpOpts[0])==gunId){
	        	        			   tmpGunName += tmpOpts[1];
	        	        			   break;
	        	        		   }
	        	        	   }
	        	        	   
	        	        	   tmpString += " "+tmpGunName;
	        	        	   
	        	        	   Score s = new Score((long)gameView.getCurrentScore(),tmpString);
	        	        	   Leaderboard l = new Leaderboard(LEADERBOARD_ID_TOPSHOOTERS);
	        	        	   
	        	        	  // s.blob = tmpGunName.getBytes();
	        	        	   
	        	        	   s.submitTo(l, new Score.SubmitToCB() {
									@Override
									public void onSuccess(boolean newHighScore) {
										//Toast.makeText(CoinShot.this, R.string.t_submitscore_success, Toast.LENGTH_LONG).show();
									}
									@Override
									public void onFailure(String exceptionMessage) {
										Toast.makeText(CoinShot.this, CoinShot.this.getString(R.string.t_submitscore_success)+" "+exceptionMessage,
															Toast.LENGTH_SHORT).show();
									}
	        	        	   });
	        	        	   
	        	        	   dialog.cancel();
	        	        	   gameView.resetGame();
	        	           }
	        	       })
	        	       .setNegativeButton(R.string.t_retry, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							gameView.resetGame();
						}
					});
	        	AlertDialog alert = builder.create();
	        	alert.show();
			}
		});
    }
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	
    	if(keyCode==KeyEvent.KEYCODE_BACK){
    		finish();
    		return true;
    	}
    	
    	return super.onKeyDown(keyCode, event);
    }
    
    /**
     * create initial cache folders and user config
     */
    public void firstRun()
    {
    	String state = Environment.getExternalStorageState();
    	
    	if(!Environment.MEDIA_MOUNTED.equals(state) &&
    	   !Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
    	{
    		Toast.makeText(this, R.string.msg_no_storage, Toast.LENGTH_LONG).show();
    	}
    	
    }
    
    public static void log(String s)
    {
    	if(s==null)
    		return;
    	Log.w("CoinShot!",s);
    }
}