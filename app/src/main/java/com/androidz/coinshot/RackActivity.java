package com.androidz.coinshot;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class RackActivity extends Activity {

	protected TextView gunName;
	protected TextView gunCaliber;
	protected TextView gunCapacity;
	protected Button bSelectGun;
	
	private RackedGun selectedGun = null;
	
	protected ImageAdapter ia;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.gunrack);
		
		gunName = (TextView)findViewById(R.id.gname);
		gunCaliber = (TextView)findViewById(R.id.gcaliber);
		gunCapacity = (TextView)findViewById(R.id.gmag);
		bSelectGun = (Button)findViewById(R.id.bselect);
		
		Gallery g = (Gallery) findViewById(R.id.gallery);
		
		g.setCallbackDuringFling(false);
		
		g.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				//Log.w("Rack","Item selected: "+arg2+", arg3: "+arg3);
				selectedGun = ia.guns.get(arg2);
				SpannableString content = new SpannableString(ia.guns.get(arg2).name);
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				gunName.setText(content);
				gunCaliber.setText(getString(R.string.t_label_caliber)+" "+ ia.guns.get(arg2).caliber);
				gunCapacity.setText(getString(R.string.t_label_capacity)+" "+ia.guns.get(arg2).capacity);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				//Log.w("Rack","Nothing Selected");
			}
		});
		
		ia = new ImageAdapter(this);
		
		String gunList[] = getResources().getStringArray(R.array.guns);
		
		for (String s : gunList) {
			String tmpOpts[] = s.split(";");
			
			RackedGun tmpGun = new RackedGun();
			tmpGun.gunzId = Integer.parseInt(tmpOpts[0]);
			tmpGun.imageId = getResources().getIdentifier("gun"+tmpOpts[0], "drawable", getPackageName());
			tmpGun.owned = false;
			tmpGun.name = tmpOpts[1];
			tmpGun.caliber = tmpOpts[2];
			tmpGun.capacity = tmpOpts[3];
			
			ia.addGun(tmpGun);
		}
		
		bSelectGun.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(selectedGun!=null){
					String checkPath = FireArms.getUniveralPackagePath(Integer.toString(selectedGun.gunzId));
					if(new File(checkPath).isDirectory()){
						Intent i = new Intent(RackActivity.this, CoinShot.class);
						i.putExtra(MenuActivity.INTENT_EXTRA_GUNID, selectedGun.gunzId);
						startActivity(i);
					} else {
						TextView tv = new TextView(RackActivity.this);
			        	tv.setText(R.string.t_buy_text);
			        	tv.setGravity(Gravity.CENTER);
			        	tv.setTextSize(19);
			        	tv.setTextColor(Color.WHITE);
			        	
			        	TextView title = new TextView(RackActivity.this);
			        	title.setText(R.string.t_buy_title);
			        	title.setGravity(Gravity.CENTER);
			        	title.setTextSize(24);
			        	title.setTextColor(Color.WHITE);
			        	
			        	AlertDialog.Builder builder = new AlertDialog.Builder(RackActivity.this);
			        	builder.setView(tv)
			        			.setCustomTitle(title)
			        	       .setCancelable(false)
			        	       .setPositiveButton(R.string.t_cancel, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
								}
							})
			        	       .setNegativeButton(R.string.t_buynow, new DialogInterface.OnClickListener() {
			        	           public void onClick(DialogInterface dialog, int id) {
			        	        	   dialog.cancel();
			        	           }
			        	       });
			        	AlertDialog alert = builder.create();
			        	alert.show();
					}
				}
			}
		});
		
		/*tmpGun2.gunzId = 2;
		tmpGun2.imageId = R.drawable.gun2;
		tmpGun2.owned = false;
		tmpGun2.name = "USPS .45";
		
		tmpGun3.gunzId = 3;
		tmpGun3.imageId = R.drawable.gun3;
		tmpGun3.owned = false;
		tmpGun3.name = "T850 Revolver";
		
		tmpGun4.gunzId = 4;
		tmpGun4.imageId = R.drawable.gun4;
		tmpGun4.owned = false;
		tmpGun4.name = "Desert Eagle";
		
		ia.addGun(tmpGun1);
		ia.addGun(tmpGun2);
		ia.addGun(tmpGun4);
		ia.addGun(tmpGun3);*/
		
	    g.setAdapter(ia);
	    g.setAnimationDuration(1500);
	    g.setUnselectedAlpha(200);
	    g.setSpacing(15);
	    g.setFadingEdgeLength(15);
	    g.setHorizontalFadingEdgeEnabled(true);
	    
	}
	
	public class RackedGun{
		public int imageId;
		public int gunzId;
		public boolean owned = false;
		public String name = "Loading..";
		public String caliber = ".45 ACP";
		public String capacity = "10";
	}
	
	public class ImageAdapter extends BaseAdapter {
	    int mGalleryItemBackground;
	    private Context mContext;

	    protected ArrayList<RackedGun> guns;
	    
	    public void addGun(RackedGun gun){
	    	guns.add(gun);
	    }
	    
	    public ImageAdapter(Context c) {
	        mContext = c;
	        //TypedArray a = c.obtainStyledAttributes(R.styleable.Gallery1);

	       // mGalleryItemBackground = R.drawable.bg2;
	       // a.recycle();
	        
	        guns = new ArrayList<RackedGun>();
	    }

	    public int getCount() {
	        return guns.size();
	    }

	    public Object getItem(int position) {
	        return position;
	    }

	    public long getItemId(int position) {
	        return position;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView i = new ImageView(mContext);
	        RackedGun tmpGun = guns.get(position);
	        

	        i.setImageResource(tmpGun.imageId);
	        i.setLayoutParams(new Gallery.LayoutParams(640,
	        											480));
	        i.setScaleType(ImageView.ScaleType.FIT_XY);
	        //i.setAlpha(95);
	        //i.setPadding(10, 0, 10, 0);
	        //i.setBackgroundResource(mGalleryItemBackground);
	        i.setSelected(false);
	        return i;
	    }
	}
	
}
