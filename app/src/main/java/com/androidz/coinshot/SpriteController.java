package com.androidz.coinshot;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;

abstract class SpriteController
{
	abstract void onDestructionCycle(Canvas c);
	abstract void onThrowCycle(Canvas c);
	abstract void onStillRenderCycle(Canvas c, Point location);
	
	public final static int DEFAULT_ANIMATION_DELAY = 25;
	public final static int DEFAULT_IMPACT_DELAY = 25;
	
	public final static float DEFAULT_OBJECT_GRAVITY = 45.f;
	public final static float DEFAULT_OBJECT_LAUNCHSPEED = 10.f;
	public final static float DEFAULT_HOVER_COUNT = 12;
	public final static float DEFAULT_SWAY_VELOCITY = 2.0f;
	
	public final static float DEFAULT_VALUE = 10.f;
	
	protected Context ctx;
	protected long mLastTime;
	protected long mLastTimeFrame;
	protected int  currentFrame = 0;
	
	protected PointF spriteLoc;
	protected Point spriteSize = null;
	
	public boolean stopAnimation = false;
	public boolean launchObject = false;
	public boolean destroyObject = false;
	public boolean isMoving = false;
	
	protected PointF spriteSpeed;
	protected PointF spriteDest;
	protected PointF spriteBaseline = new PointF(150.f,350.f);
	protected float  spriteFriction = 0.98f;

	protected int   hoverCount = 0;
	
	protected int animationDelay = DEFAULT_ANIMATION_DELAY;
	
	protected float impactRotation = 0.f;
	
	protected Point impactOffset = null;
	protected Point impactSize = null;
	
	public SpriteController(Context c) {
		ctx = c;
		
		spriteLoc = new PointF(spriteBaseline.x, spriteBaseline.y);
		spriteSpeed = new PointF(0.0f,DEFAULT_OBJECT_LAUNCHSPEED);
		spriteDest = new PointF(150.f,20.f);
		
		impactSize = new Point(0,0);
		impactOffset = new Point(0,0);
		
		mLastTime = System.currentTimeMillis() + 100;
	}
	
	public void destroy(){
		stopAnimation = true;
	}
	
	public float getValue()
	{
		return DEFAULT_VALUE;
	}
	
	protected void loadImpactAnimation()
	{
		
	}
	
	public boolean isMoving()
	{
		return isMoving;
	}
	
	public void configure(PointF start, PointF speed, PointF dest)
	{
		spriteLoc = start;
		spriteSpeed = speed;
		spriteBaseline.y = start.y;
		spriteBaseline.x = start.x;
		spriteDest = dest;
	}
	
	public void setCoinSpeed(PointF speed)
	{
		spriteSpeed = speed;
	}
	
	abstract public void setCoinSize(int size);
	
	public void setCoinFriction(float friction)
	{
		spriteFriction = friction;
	}
	
	public void setDestination(PointF dest)
	{
		spriteDest = dest;
	}
	
	public void setLaunchPoint(float x){
		spriteLoc.x = x;
		spriteBaseline.x = x;
	}
	
	public boolean launch()
	{
		impactRotation = (float)(Math.random()*360);
		launchObject = true;
		return true;
	}
	
	public void impact()
	{
		if(destroyObject)
			return;
		
		destroyObject = true;
		currentFrame = 0;
		isMoving = false;
	}
	
	public void renderInPlace(Canvas c, Point location)
	{
		long now = System.currentTimeMillis();
    	double elapsed = (now - mLastTime);
    	mLastTimeFrame += elapsed;
    	elapsed = elapsed/1000.0;
    	
    	if(mLastTimeFrame>=animationDelay){
    		currentFrame++;
    		mLastTimeFrame = 0;
    	}
    	
    	onStillRenderCycle(c,location);
    	
    	mLastTime = System.currentTimeMillis();
	}
	
	public void render(Canvas c)
	{
		if(stopAnimation)
			return;
		
		long now = System.currentTimeMillis();
    	double elapsed = (now - mLastTime);
    	mLastTimeFrame += elapsed;
    	elapsed = elapsed/1000.0;
    	
    	if(destroyObject){
    		
    		onDestructionCycle(c);
    		
    		if(mLastTimeFrame>=DEFAULT_IMPACT_DELAY){
        		currentFrame++;
        		mLastTimeFrame = 0;
        	}
    		
    		mLastTime = System.currentTimeMillis();
    		return;
    	}
    	
    	if(mLastTimeFrame>=animationDelay){
    		currentFrame++;
    		mLastTimeFrame = 0;
    	}
    	
    	onThrowCycle(c);
    	
    	if(isMoving){
    		if(spriteSpeed.x!=0.0f){
    			spriteLoc.x += spriteSpeed.x;
    			//coinLoc.x = (coinSpeed.x*DEFAULT_SWAY_VELOCITY)*coinFriction;
    		}
    	}
    	
    	if(launchObject)
		{
			//coinFriction = (float)(coinFriction*0.65);
			//launchTest = false;
    		isMoving = true;
			if(spriteLoc.y<=spriteDest.y)
			{
				launchObject = false;
			} else {
				spriteLoc.y -= (spriteSpeed.y);
				//coinSpeed.y = coinSpeed.y*coinFriction;
			}
		}
		else{
			if(spriteLoc.y<spriteBaseline.y){
				
				if(hoverCount<=DEFAULT_HOVER_COUNT){
					//if(i==(aCoin.length-1)){
						hoverCount++;
					//}
					spriteLoc.y += (1.0f);
				} else {
					if(spriteSpeed.y<DEFAULT_OBJECT_LAUNCHSPEED){
						if(spriteSpeed.y < DEFAULT_OBJECT_GRAVITY)
							spriteSpeed.y += (DEFAULT_OBJECT_LAUNCHSPEED*0.75);
					} 
					spriteLoc.y += (spriteSpeed.y);
				}
			}
			else if(spriteLoc.y>=spriteBaseline.y){
				//reset coin for next reconfig and launch 
				resetOptions();
			}
		}
    	
		mLastTime = System.currentTimeMillis();
	}

	public void resetOptions()
	{
		spriteSpeed.y=DEFAULT_OBJECT_LAUNCHSPEED;
		spriteSpeed.x = 0.0f;
		spriteLoc.y = spriteBaseline.y;
		spriteLoc.x = spriteBaseline.x;
		spriteFriction = 0.98f;
		hoverCount = 0;
		launchObject = false;
		destroyObject = false;
		isMoving = false;
	}
	
	public void setCoinDelay(int coinDelay) {
		this.animationDelay = coinDelay;
	}

	public int getCoinDelay() {
		return animationDelay;
	}
}
