package com.androidz.coinshot;

import com.openfeint.api.ui.Dashboard;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MenuActivity extends Activity {

	public static final String INTENT_EXTRA_GUNID = "INTENT_EXTRA_GUNID"; 
	private static final String PREF_FILE = "settings.dat";
	public static final String USERKEY_MYGUNID = "mygunid";
	public static final String USERKEY_FIRSTRUN = "csfirstrun";
	public static final int GUNID_DEFAULT = -1;
	
	private SharedPreferences userConfig;
	
	private int myGun;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setTheme(android.R.style.Theme_Light_NoTitleBar);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		getWindow().makeActive();
        getWindow().setFormat(PixelFormat.TRANSPARENT);
        
        setContentView(R.layout.mainmenu);
        
        userConfig = getApplicationContext().getSharedPreferences(PREF_FILE, 0);
        
        myGun = userConfig.getInt(PREF_FILE, GUNID_DEFAULT);
        
        if(userConfig.getBoolean(USERKEY_FIRSTRUN, true)){
        	
        	TextView tv = new TextView(this);
        	tv.setText(R.string.t_disclaimer);
        	tv.setGravity(Gravity.CENTER);
        	tv.setTextSize(19);
        	tv.setTextColor(Color.WHITE);
        	
        	TextView title = new TextView(this);
        	title.setText(R.string.t_disclaimer_title);
        	title.setGravity(Gravity.CENTER);
        	title.setTextSize(24);
        	title.setTextColor(Color.WHITE);
        	
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setView(tv)
        			.setCustomTitle(title)
        	       .setCancelable(false)
        	       .setPositiveButton(R.string.t_label_letsgo, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	        	   checkDefaultGun();
        	           }
        	       });
        	AlertDialog alert = builder.create();
        	alert.show();
        	
        } else {
        	checkDefaultGun();
        }
        
        Button bPlay = (Button)findViewById(R.id.bplay);
        bPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkDefaultGun())
				{
					Intent i = new Intent(MenuActivity.this, CoinShot.class);
					i.putExtra(INTENT_EXTRA_GUNID, myGun);
					startActivity(i);
				}
			}
		});
        
        Button bRack = (Button)findViewById(R.id.brack);
        bRack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MenuActivity.this, RackActivity.class));
			}
		});
        
        Button bFeint = (Button)findViewById(R.id.bfeint);
        bFeint.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Dashboard.openLeaderboards();
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if(keyCode==KeyEvent.KEYCODE_BACK){
			finish();
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	private boolean checkDefaultGun(){
		if(myGun==GUNID_DEFAULT){
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setMessage(R.string.t_firstgun_message)
        		   .setTitle(R.string.t_firstgun_title)
        	       .setCancelable(false)
        	       .setPositiveButton(R.string.t_yes, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	        	   startActivity(new Intent(MenuActivity.this, RackActivity.class));
        	           }
        	       })
        	       .setNegativeButton(R.string.t_no, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                dialog.cancel();
        	           }
        	       });
        	AlertDialog alert = builder.create();
        	alert.show();
        	return false;
        } else {
        	return true;
        }
	}
	
}
